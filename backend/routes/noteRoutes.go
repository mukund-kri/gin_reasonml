package routes

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	c "gitlab.com/mukund-kri/gin_reasonml/backend/config"
	"gitlab.com/mukund-kri/gin_reasonml/backend/models"
)

// Handles deleting Notes from a notebook
func addNote(ctx *gin.Context) {

	notebookID, _ := strconv.Atoi(ctx.Param("notebookId"))
	var newNote models.Note

	if err := ctx.BindJSON(&newNote); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err})
	} else {
		newNote.NoteBookID = uint(notebookID)
		c.DB.Debug().Save(&newNote)
		ctx.JSON(200, gin.H{"data": newNote})
	}
}

// Handles deleting notes from a notebook
func deleteNote(ctx *gin.Context) {
	id := ctx.Param("id")
	var note models.Note

	c.DB.Delete(&note, id)
	ctx.JSON(200, gin.H{"data": note})
}

func AttachNoteRoutes(engine *gin.Engine) {

	notebookAPI := engine.Group("/notebook")
	{
		notebookAPI.POST("/:notebookId/notes", addNote)
		notebookAPI.DELETE("/:notebookId/notes/:id", deleteNote)
		// notebookAPI.PUT("/:notebookId/note/:id", updateNote)
	}

}
