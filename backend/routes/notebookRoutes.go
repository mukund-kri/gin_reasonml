package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"

	c "gitlab.com/mukund-kri/gin_reasonml/backend/config"
	"gitlab.com/mukund-kri/gin_reasonml/backend/models"
)

func allNotebooks(ctx *gin.Context) {
	var allNotebooks []models.NoteBook
	c.DB.Find(&allNotebooks)
	ctx.JSON(200, gin.H{"data": allNotebooks})
}

func notebookByID(ctx *gin.Context) {
	id := ctx.Param("id")
	var noteBook models.NoteBook

	c.DB.Preload("Notes").First(&noteBook, id)

	if noteBook.ID == 0 {
		ctx.JSON(404, gin.H{"data": "wrong id"})
	} else {
		ctx.JSON(200, gin.H{"data": noteBook})
	}
}

func addNotebook(ctx *gin.Context) {
	var newNotebook models.NoteBook
	if err := ctx.BindJSON(&newNotebook); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err})
	} else {
		c.DB.Save(&newNotebook)
		ctx.JSON(200, gin.H{"data": newNotebook})
	}
}

func deleteNotebook(ctx *gin.Context) {
	id := ctx.Param("id")
	var noteBook models.NoteBook

	c.DB.Delete(&noteBook, id)
	ctx.JSON(200, gin.H{"data": noteBook})

}

func updateBook(ctx *gin.Context) {
	id := ctx.Param("id")
	var noteBook models.NoteBook
	c.DB.Find(&noteBook, id)
	if noteBook.ID == 0 {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "notebook not found"})
		return
	}

	if err := ctx.BindJSON(&noteBook); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err})
	} else {
		c.DB.Save(&noteBook)
		ctx.JSON(200, gin.H{"data": noteBook})
	}
}

// AttachNotebookRoutes attaches all the routes for the notebooks to the gin
// engine
func AttachNotebookRoutes(engine *gin.Engine) {
	notebookAPI := engine.Group("/notebooks")
	// notebookAPI.Use(middleware.TokenAuthMiddleware())
	{
		notebookAPI.GET("/", allNotebooks)
		notebookAPI.GET("/:id", notebookByID)
		notebookAPI.POST("/", addNotebook)
		notebookAPI.DELETE("/:id", deleteNotebook)
		notebookAPI.PUT("/:id", updateBook)
	}
}
