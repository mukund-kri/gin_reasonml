/*
	This file holds all the routes needed for auth.
*/

package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/mukund-kri/gin_reasonml/backend/models"
)

// EmailJSON is a struct to deserialize the JSON for email exists query
type EmailJSON struct {
	Email string `json:"email" validator:"required,email"`
}

func emailExists(ctx *gin.Context) {

	var emailJSON EmailJSON
	if err := ctx.BindJSON(&emailJSON); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid_json"})
	} else {
		emailExists := models.EmailExists(emailJSON.Email)
		ctx.JSON(http.StatusOK, gin.H{"email_exist": emailExists})
	}
}

// signup a new user into the system
func signUp(ctx *gin.Context) {

	var newUser models.SignupParams
	if err := ctx.BindJSON(&newUser); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		err := models.SignupUser(&newUser)
		if err != nil {
			ctx.JSON(500, gin.H{"error": err.Error()})
		} else {
			ctx.JSON(http.StatusOK, gin.H{"data": newUser})
		}
	}
}

func login(ctx *gin.Context) {

	var auth models.AuthCredentials
	if err := ctx.BindJSON(&auth); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error})
		return
	}

	user, err := models.Authenticate(auth)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{"error": "NO_Login"})
		return
	}

	tokenStr, _ := models.GenerateToken(user.Email)

	ctx.JSON(http.StatusOK, gin.H{"jwt": tokenStr})
}

// AttachAuthRoutes : attaches all the handler connected with auth to the
// gin engine
func AttachAuthRoutes(engine *gin.Engine) {

	authAPI := engine.Group("/auth")
	{
		authAPI.POST("/email_exists", emailExists)
		authAPI.POST("/signup", signUp)
		authAPI.POST("/login", login)
	}
}
