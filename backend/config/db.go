package config

import (
	"fmt"
	"log"
	"strconv"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	dbUser     = "assignment"
	dbPassword = "assignment"
	dbHost     = "127.0.0.1"
	dbPort     = 5432
	dbName     = "notes"
)

// DB - Connection to the database for the whole application.
var DB *gorm.DB

// InitializeDB : initializes the connection to postgres. This connection is
// used throughout the application.
func InitializeDB() {
	dsn := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s dbname=%s",
		EnvOrDefault("DB_USER", dbUser),
		EnvOrDefault("DB_PASSWORD", dbPassword),
		EnvOrDefault("DB_HOST", dbHost),
		EnvOrDefault("DB_PORT", strconv.Itoa(dbPort)),
		EnvOrDefault("DB_NAME", dbName),
	)

	log.Println("Connecting to DB with: ", dsn)
	var err error
	DB, err = gorm.Open(
		postgres.Open(dsn),
		&gorm.Config{},
	)
	if err != nil {
		log.Fatal("Error in opening db. Exiting ", err)
	}

	log.Println("Database initialized successfully")
}
