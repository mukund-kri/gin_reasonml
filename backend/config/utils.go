package config

import "os"

// EnvOrDefault : tries to get the environment variable, if the env var does
// not exists it returns the default value
func EnvOrDefault(envName string, defaultValue string) string {
	value, exits := os.LookupEnv(envName)
	if exits {
		return value
	}
	return defaultValue
}
