package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/mukund-kri/gin_react/backend/config"
	"gitlab.com/mukund-kri/gin_react/backend/models"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Println("[WARN] Error opening env file. Using default config values")
	}
}

func main() {

	// Create connection to DB
	config.InitializeDB()

	config.DB.AutoMigrate(
		&models.NoteBook{},
		&models.Note{},
		&models.User{},
	)
}
