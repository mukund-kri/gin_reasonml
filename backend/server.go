package main

import (
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"gitlab.com/mukund-kri/gin_reasonml/backend/config"
	"gitlab.com/mukund-kri/gin_reasonml/backend/routes"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Println("[WARN] Error opening env file. Using default config values")
	}

}

func main() {
	server := gin.Default()

	// Create connection to DB
	config.InitializeDB()

	// Open all ports for CORS !!!ONLY FOR LEARNING!!!
	server.Use(cors.Default())

	// Attach all handlers related Auth API
	routes.AttachAuthRoutes(server)

	// Attach Notes API
	routes.AttachNoteRoutes(server)

	// Attach all routes for NoteBook CRUD
	routes.AttachNotebookRoutes(server)

	server.Run()
}
