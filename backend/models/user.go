package models

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"

	"gitlab.com/mukund-kri/gin_reasonml/backend/config"
)

var secretKey = []byte("skfshdfj sdjfhjsfhsd")

// User model is used for auth and user profile
type User struct {
	gorm.Model

	// Also the user name for authentication
	Email string `json:"email" validator:"required,email" gorm:"unique"`

	// The world should never see the encrypted password
	Password string `json:"-"`
	FullName string `json:"full_name" validator:"required"`

	// For account management
	IsActive bool `json:"is_active" gorm:"default:true"`
}

type SignupParams struct {
	Email    string `json:"email" validator:"required,email"`
	Password string `json:"password" validator:"required"`
	FullName string `json:"full_name" validator:"required"`
}

// AuthCredentials holds the email and password. The two parameters required to
// authenticate a user.
type AuthCredentials struct {
	Email    string `json:"email" validator:"required,email"`
	Password string `json:"password" validator:"required"`
}

// EmailExists : check if email already used
func EmailExists(email string) bool {

	var user User
	err := config.DB.Where("email = ?", email).First(&user).Error

	if err != nil {
		return false
	} else {
		return true
	}
}

// SignupUser : Add a new user to our app
func SignupUser(signupParams *SignupParams) error {

	var newUser User
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(signupParams.Password), 8)
	if err != nil {
		return err
	}

	newUser.Password = string(hashedPassword)
	newUser.Email = signupParams.Email
	newUser.FullName = signupParams.FullName

	fmt.Println(newUser, signupParams.Password)

	if err := config.DB.Create(&newUser); err != nil {
		return err.Error
	}

	return nil
}

// Authenticate : Given a email, password pair in the form of AuthParams struct
// authenticate that user
func Authenticate(creds AuthCredentials) (*User, error) {

	// retrieve user from db
	var user User
	config.DB.Debug().Where("email = ?", creds.Email).First(&user)

	fmt.Println(user.Password)

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(creds.Password)); err != nil {
		fmt.Println(err)
		return nil, err
	}
	return &user, nil
}

func GenerateToken(email string) (string, error) {
	claims := jwt.MapClaims{
		"user_id": email,
		"expiry":  time.Now().Add(time.Minute * 30),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString(secretKey)
}

func ValidateToken(tokenString string) (*jwt.Token, error) {

	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return secretKey, nil
	})
}
