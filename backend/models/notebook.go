package models

import "gorm.io/gorm"

// NoteBook : entity that contains many Notes
type NoteBook struct {
	gorm.Model
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"`
	Notes       []Note
}

// Note : entity to store individual notes
type Note struct {
	ID         uint   `gorm:"primaryKey"`
	Text       string `json:"text" binding:"required"`
	NoteBookID uint
}
