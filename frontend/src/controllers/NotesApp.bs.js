'use strict';

var Json = require("@glennsl/bs-json/src/Json.bs.js");
var React = require("react");
var Models$Notesapp = require("../models/Models.bs.js");

function NotesApp(Props) {
  React.useState(function () {
        
      });
  React.useEffect((function () {
          fetch("http://localhost:8080/notebooks/1").then(function (prim) {
                    return prim.text();
                  }).then(function (j) {
                  return Promise.resolve(Models$Notesapp.decodeNotebook(Json.parseOrRaise(j)));
                }).then(function (note) {
                return Promise.resolve((console.log(note.title), undefined));
              });
          
        }), []);
  return React.createElement(React.Fragment, undefined, React.createElement("h1", undefined, "hello world"));
}

var make = NotesApp;

exports.make = make;
/* react Not a pure module */
