[@react.component]
let make = () => {
    open Models;

    let (note, setNote) = React.useState(_ => None);
    
    React.useEffect0(() => {
        Js.Promise.(
            Fetch.fetch("http://localhost:8080/notebooks/1")
            |> then_(Fetch.Response.text)
            |> then_(j => j |> Json.parseOrRaise |> Models.decodeNotebook |> resolve)
            |> then_(note => print_endline(note.title) |> resolve)
            |> ignore
        );
        None
    });

    
    <>
        <h1>{React.string("hello world")}</h1>
    </>
}