'use strict';

var React = require("react");
var ReactDom = require("react-dom");
var NotesApp$Notesapp = require("./controllers/NotesApp.bs.js");

var root = document.querySelector("#root");

if (!(root == null)) {
  ReactDom.render(React.createElement(NotesApp$Notesapp.make, {}), root);
}

/* root Not a pure module */
