'use strict';

var Json_decode = require("@glennsl/bs-json/src/Json_decode.bs.js");
var Json_encode = require("@glennsl/bs-json/src/Json_encode.bs.js");

function decodeNote(json) {
  return {
          text: Json_decode.field("text", Json_decode.string, json)
        };
}

function encodeNote(note) {
  return Json_encode.object_({
              hd: [
                "text",
                note.text
              ],
              tl: /* [] */0
            });
}

function decodeNotebook(json) {
  return {
          title: Json_decode.field("title", Json_decode.string, json),
          descrption: Json_decode.field("description", Json_decode.string, json),
          notes: Json_decode.field("notes", (function (param) {
                  return Json_decode.array(decodeNote, param);
                }), json)
        };
}

exports.decodeNote = decodeNote;
exports.encodeNote = encodeNote;
exports.decodeNotebook = decodeNotebook;
/* No side effect */
