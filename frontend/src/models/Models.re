type note = {
    text: string
};

type notebook = {
    title: string,
    descrption: string,
    notes: array(note)
};

let decodeNote = json =>
    Json.Decode.{
        text: json |> field("text", string)
    };

let encodeNote = note =>
    Json.Encode.(
        object_ ([
            ("text", string(note.text))
        ])
    );

let decodeNotebook = json =>
    Json.Decode.{
        title: json |> field("title", string),
        descrption: json |> field("description", string),
        notes: json |> field("notes", array(decodeNote))
    }

